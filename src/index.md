# MoeNavigator

MoeNavigator is an attempt to write a web browser that is highly configurable,
secure, anonymous and fast. It is not another GUI for Chromium or a browser
engine like WebKit or Gecko. Instead, it has its own browser engine called
MoeNavigatorEngine that has been written from scratch.

## Quick overview

- License: GPLv3+
- Mainly written in C++17
- GUI via Qt5
- No spying or tracking


## Repositories

There are three source code repositories:

- [MoeNavigator itself](https://codeberg.org/moenavigator/moenavigator)
- [MoeNavigatorEngine, the browser engine](https://codeberg.org/moenavigator/moenavigatorengine)
- [Tools for the engine](https://codeberg.org/moenavigator/moenavigatorengine-tools)

The easiest way to use MoeNavigator is to clone its repository and follow
the instructions in the README.md file to compile it.
